package com.example.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	// test
	@GetMapping("/")
	public String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@GetMapping("/hello/{name}")
	public String hello(@PathVariable String name) {
		return "Hello " + name + "!";
	}

	// Generate a GET method that checks whether first parameter is present in a
	// MySql database called 'names', create variables to connect to it via a jdbc
	// string

	String url = "jdbc:mysql://sql11.freemysqlhosting.net:3306/sql11677286";
	String username = "sql11677286";
	String myPassw = System.getenv("PASSWORD");
	@GetMapping("/names/{name}")
	public String checkName(@PathVariable String name) {
		// Connect to database and check if name exists
		if (myPassw == null) {
			throw new RuntimeException("PASSWORD environment variable is not set");
		}

		try {
			Connection conn = DriverManager.getConnection(url, username, myPassw);
			Statement stmt = conn.createStatement();
			String sql = "SELECT COUNT(*) FROM names WHERE name = '" + name + "'";
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next() && rs.getInt(1) > 0) {
				return name + " exists in the database";
			} else {
				return name + " does not exist in the database";
			}
		} catch (SQLException e) {
			return "Error checking database: " + e.getMessage();
		}

		// Return result
	}

}
